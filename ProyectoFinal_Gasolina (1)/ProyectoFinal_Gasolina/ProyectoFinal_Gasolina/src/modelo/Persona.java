
package modelo;

import java.util.Date;
import modelo.User.Administrador;
import modelo.User.Despachador;
import modelo.enums.EstadoEmpleado;
import modelo.enums.TipoEmpleado;
import modelo.enums.TipoIdentificacion;

/**
 * Pablo Lopez, Cristian Armijos y Leonardo Peralta
 */
public class Persona {
    private Integer id;
    private String nombres;
    private String apellidos;
    private String identificacion;
    private TipoIdentificacion tipo_identificacion;
    private EstadoEmpleado estadoEmpleado;
    private TipoEmpleado tipoempleado;
    private Despachador despachador;
    private Administrador admin;
    private Despachador despa;

    public EstadoEmpleado getEstadoEmpleado() {
        return estadoEmpleado;
    }

    public void setEstadoEmpleado(EstadoEmpleado estadoEmpleado) {
        this.estadoEmpleado = estadoEmpleado;
    }

    public TipoEmpleado getTipoempleado() {
        return tipoempleado;
    }

    public void setTipoempleado(TipoEmpleado tipoempleado) {
        this.tipoempleado = tipoempleado;
    }

    public Despachador getDespachador() {
        return despachador;
    }

    public void setDespachador(Despachador despachador) {
        this.despachador = despachador;
    }

    public Administrador getAdmin() {
        return admin;
    }

    public void setAdmin(Administrador admin) {
        this.admin = admin;
    }

    public Despachador getDespa() {
        return despa;
    }

    public void setDespa(Despachador despa) {
        this.despa = despa;
    }
    
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public TipoIdentificacion getTipo_identificacion() {
        return tipo_identificacion;
    }

    public void setTipo_identificacion(TipoIdentificacion tipo_identificacion) {
        this.tipo_identificacion = tipo_identificacion;
    }

}
