package modelo;

import java.util.Date;
import modelo.enums.TipoIdentificacion;


/**
 * Pablo Lopez, Cristian Armijos y Leonardo Peralta
 */
public class Cliente extends Persona{
    private String identificacion;
    private String telefono;
    private TipoIdentificacion tipo_identificacion;

    public TipoIdentificacion getTipo_identificacion() {
        return tipo_identificacion;
    }

    public void setTipo_identificacion(TipoIdentificacion tipo_identificacion) {
        this.tipo_identificacion = tipo_identificacion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    
    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

   
    
}