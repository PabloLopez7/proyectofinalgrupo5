/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modelo;

import modelo.enums.EstadoEmpleado;
import modelo.enums.TipoEmpleado;


/**
 * Pablo Lopez, Cristian Armijos y Leonardo Peralta
 */
public class Empleado extends Persona{
    
    private Double sueldo;
    private TipoEmpleado tipoEmp;
    private EstadoEmpleado estado;

    public Double getSueldo() {
        return sueldo;
    }

    public void setSueldo(Double sueldo) {
        this.sueldo = sueldo;
    }

    public TipoEmpleado getTipoEmp() {
        return tipoEmp;
    }

    public void setTipoEmp(TipoEmpleado tipoEmp) {
        this.tipoEmp = tipoEmp;
    }

    public EstadoEmpleado getEstado() {
        return estado;
    }

    public void setEstado(EstadoEmpleado estado) {
        this.estado = estado;
    }
    
    
}
