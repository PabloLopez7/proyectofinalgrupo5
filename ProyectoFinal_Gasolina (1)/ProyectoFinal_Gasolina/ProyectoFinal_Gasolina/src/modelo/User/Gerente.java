
package modelo.User;

/**
 * Pablo Lopez, Cristian Armijos y Leonardo Peralta
 */
public class Gerente {
    private String Usuario="gerente1104";
    private String Contra="gerentecontra";

    public String getUsuario() {
        return Usuario;
    }

    public void setUsuario(String Usuario) {
        this.Usuario = Usuario;
    }

    public String getContra() {
        return Contra;
    }

    public void setContra(String Contra) {
        this.Contra = Contra;
    }
    
    
}
