
package modelo.User;

import modelo.enums.TipoEmpleado;
import modelo.enums.TipoIdentificacion;


/**
 * Pablo Lopez, Cristian Armijos y Leonardo Peralta
 */
public class Administrador {
    private String Usuario="admin1104";
    private String Contra="admincontra";
    private String nombre;
    private String apellido;
    private String id;
    private String contacto;
    private TipoIdentificacion tipoIdent;
    private TipoEmpleado tipoempleado;
    private String Domicilio;

    public String getUsuario() {
        return Usuario;
    }

    public void setUsuario(String Usuario) {
        this.Usuario = Usuario;
    }

    public String getContra() {
        return Contra;
    }

    public void setContra(String Contra) {
        this.Contra = Contra;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public TipoIdentificacion getTipoIdent() {
        return tipoIdent;
    }

    public void setTipoIdent(TipoIdentificacion tipoIdent) {
        this.tipoIdent = tipoIdent;
    }

    public TipoEmpleado getTipoempleado() {
        return tipoempleado;
    }

    public void setTipoempleado(TipoEmpleado tipoempleado) {
        this.tipoempleado = tipoempleado;
    }

    public String getDomicilio() {
        return Domicilio;
    }

    public void setDomicilio(String Domicilio) {
        this.Domicilio = Domicilio;
    }
    
    
}
