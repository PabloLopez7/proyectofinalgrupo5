
package modelo.enums;


/**
 * Pablo Lopez, Cristian Armijos y Leonardo Peralta
 */
public enum TipoIdentificacion {
    CEDULA, PASAPORTE;
}
