/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

import java.util.Date;

/**
 * Pablo Lopez, Cristian Armijos y Leonardo Peralta
 */
public class Detalle_producto {
    private Integer id;
    private String nombre;
    private Boolean estado;
    private Double p_compra;
    private Double p_venta;
    private Date createdAt;
    private Date updatedAt;
    private String external_id;
    private Integer id_categoria;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public Double getP_compra() {
        return p_compra;
    }

    public void setP_compra(Double p_compra) {
        this.p_compra = p_compra;
    }

    public Double getP_venta() {
        return p_venta;
    }

    public void setP_venta(Double p_venta) {
        this.p_venta = p_venta;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getExternal_id() {
        return external_id;
    }

    public void setExternal_id(String external_id) {
        this.external_id = external_id;
    }

    public Integer getId_categoria() {
        return id_categoria;
    }

    public void setId_categoria(Integer id_categoria) {
        this.id_categoria = id_categoria;
    }
    
}
