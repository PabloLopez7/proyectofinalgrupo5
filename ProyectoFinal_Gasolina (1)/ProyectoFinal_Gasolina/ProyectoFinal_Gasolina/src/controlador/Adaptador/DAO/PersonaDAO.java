package controlador.Adaptador.DAO;

import controlador.Adaptador.AdaptadorDao;
import controlador.tda.lista.ListaEnlazada;
import modelo.Persona;
import modelo.enums.TipoIdentificacion;

/**
 * Pablo Lopez, Cristian Armijos y Leonardo Peralta
 */

public class PersonaDAO extends AdaptadorDao<Persona> {

    private Persona persona;

    public Persona getPersona() {
        if (persona == null) {
            persona = new Persona();
        }
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public PersonaDAO() {
        super(Persona.class);
    }

    public Boolean guardar_modificar() {
        try {
            if (getPersona().getId() != null) {
                modificar(this.getPersona());
            } else {
                guardar(this.getPersona());
            }
            return true;
        } catch (Exception e) {
            System.out.println("Error en guadar o modificar");
            return false;
        }
    }
}
