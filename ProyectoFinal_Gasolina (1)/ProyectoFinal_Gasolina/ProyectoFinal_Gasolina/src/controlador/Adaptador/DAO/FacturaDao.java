
package controlador.Adaptador.DAO;

import controlador.Adaptador.AdaptadorDao;
import modelo.Factura;

/**
 * Pablo Lopez, Cristian Armijos y Leonardo Peralta
 */
public class FacturaDao extends AdaptadorDao<Factura>{
    private Factura factura;

    public FacturaDao() {
        super(Factura.class);
    }

    public Factura getFactura() {
        if (factura == null) {
            factura = new Factura();
        }
        return factura;
    }

    public void setFactura(Factura factura) {
        this.factura = factura;
    }

    public Boolean guardar_modificar() {
        try {
            if (getFactura().getID() != null) {
                //
                modificar(this.getFactura());
            } else {
                guardar(this.getFactura());
            }
            return true;
        } catch (Exception e) {
            System.out.println("Error en guadar o modificar");
            return false;
        }
    }
}
