
package controlador.Adaptador.DAO;

import controlador.Adaptador.AdaptadorDao;
import modelo.Categoria;

/**
 * Pablo Lopez, Cristian Armijos y Leonardo Peralta
 */
public class CategoriaDao extends AdaptadorDao<Categoria>{
    private Categoria categoria;

    public CategoriaDao() {
        super(Categoria.class);
    }

    public Categoria getCategoria() {
        if(categoria == null)
            categoria = new Categoria();
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }
    
    public Boolean guardar_modificar() {
       try {
           if(getCategoria().getId() != null) {
               //
               modificar(this.getCategoria());
           } else {
               guardar(this.getCategoria());
           }
           return true;
       } catch (Exception e) {
           System.out.println("Error en guadar o modificar");
           return false;
       }
   }
}
