
package controlador.Adaptador.DAO;

import controlador.Adaptador.AdaptadorDao;
import modelo.Empleado;

/**
 * Pablo Lopez, Cristian Armijos y Leonardo Peralta
 */
public class EmpleadoDAO extends AdaptadorDao<Empleado> {

    private Empleado vendedor;

    public Empleado getVendedor() {
        if (vendedor == null) {
            vendedor = new Empleado();
        }
        return vendedor;
    }

    public void setVendedor(Empleado vendedor) {
        this.vendedor = vendedor;
    }

    public EmpleadoDAO() {
        super(Empleado.class);
    }

    public Boolean guardar_modificar() {
        try {
            if (getVendedor().getId() != null) {
                modificar(this.getVendedor());
            } else {
                guardar(this.getVendedor());
            }
            return true;
        } catch (Exception e) {
            System.out.println("Error en guadar o modificar");
            return false;
        }
    }
}
   

