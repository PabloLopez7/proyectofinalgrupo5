
package controlador.Adaptador.DAO;

import controlador.Adaptador.AdaptadorDao;
import controlador.tda.lista.ListaEnlazada;
import modelo.Cuenta;


/**
 * Pablo Lopez, Cristian Armijos y Leonardo Peralta
 */
public class CuentaDao extends AdaptadorDao<Cuenta> {
    private Cuenta cuenta;

    public Cuenta getCuenta() {
        if (cuenta == null) {
            cuenta = new Cuenta();
        }
        return cuenta;
    }

    public void setCuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
    }

    public CuentaDao() {
        super(Cuenta.class);
    }

    public Boolean guardar_modificar() {
        try {
            if (getCuenta().getId() != null) {
                //
                modificar(this.getCuenta());
            } else {
                guardar(this.getCuenta());
            }
            return true;
        } catch (Exception e) {
            System.out.println("Error en guadar o modificar");
            return false;
        }
    }
    
    public Cuenta obtenerCuentaPersona(Integer id) {
        Cuenta c = null;
        try{
            ListaEnlazada<Cuenta> lista = listar().buscar("id_persona", id);
            c = lista.obtenerDato(0);
        }catch(Exception e){
            System.out.println("Error en obtener cuenta de persona");
        }
        return c;
    }
}
