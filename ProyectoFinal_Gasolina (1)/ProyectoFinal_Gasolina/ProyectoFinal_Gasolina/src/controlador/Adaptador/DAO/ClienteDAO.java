package controlador.Adaptador.DAO;

import controlador.Adaptador.AdaptadorDao;
import controlador.tda.lista.ListaEnlazada;
import modelo.Cliente;

/**
 * Pablo Lopez, Cristian Armijos y Leonardo Peralta
 */
public class ClienteDAO extends AdaptadorDao<Cliente> {

    private Cliente cliente;

    public Cliente getCliente() {
        if (cliente == null) {
            cliente = new Cliente();
        }
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public ClienteDAO() {
        super(Cliente.class);
    }

    public Boolean guardar_modificar() {
        try {
            if (getCliente().getId() != null) {
                modificar(this.getCliente());
            } else {
                guardar(this.getCliente());
            }
            return true;
        } catch (Exception e) {
            System.out.println("Error en guadar o modificar");
            return false;
        }
    }


}
