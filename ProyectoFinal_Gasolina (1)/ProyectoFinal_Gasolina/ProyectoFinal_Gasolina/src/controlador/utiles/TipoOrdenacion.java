/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.utiles;


/**
 * Pablo Lopez, Cristian Armijos y Leonardo Peralta
 */
public enum TipoOrdenacion {
    ASCENDENTE, DESCENDENTE;
}
