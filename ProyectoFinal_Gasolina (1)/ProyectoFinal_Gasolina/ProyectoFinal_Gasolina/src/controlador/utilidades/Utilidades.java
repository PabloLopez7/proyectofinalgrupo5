
package controlador.utilidades;

import modelo.enums.TipoIdentificacion;

/**
 *
 * @author Leonardo Peralta
 */
public class Utilidades {
    public static String [] tipoIdentificacion(){
        TipoIdentificacion[] tipos = TipoIdentificacion.values();
        String [] tiposI = new String[tipos.length];
        int i=0;
        for(TipoIdentificacion aux: tipos){
            tiposI[i] = aux.toString();
            i++;
        }
        return tiposI;
    }
    
    public static TipoIdentificacion getTipo(String aux){
        if(TipoIdentificacion.CEDULA.toString().equals(aux)){
            return TipoIdentificacion.CEDULA;
        }else{
            return TipoIdentificacion.PASAPORTE;
        }
    }
}